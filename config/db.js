const { Sequelize } = require('sequelize');

const DATABASE = process.env.DATABASE || 'sebet';
const USERNAME = process.env.USERNAME || 'postgres';
// const PASSWORD = process.env.PASSWORD || '2005';
const PASSWORD = process.env.PASSWORD || 'samsyk1902';
// const HOST = process.env.HOST || '127.0.0.1';
const HOST = process.env.HOST || 'localhost';

const db = new Sequelize(DATABASE, USERNAME, PASSWORD, {
  host: HOST,
  dialect: 'postgres',
});

module.exports = db;
