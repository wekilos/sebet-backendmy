const express = require('express');
const cache = require("../../../config/node-cache");
const {
  getAllOrders,
  getStatistics,
  getOrderProducts,
  changeOrderStatus,
  searchOrders,
  deleteOrderProduct,
} = require('../../../controllers/admin/ordersControllers');

const router = express.Router();

router.get('/',cache.get, getAllOrders, cache.set);
router.delete('/order-products/delete/:id', deleteOrderProduct);
router.get('/order-products/:id', getOrderProducts);
router.post('/change-order-status/:id', changeOrderStatus);

module.exports = router;
